Xorm is a simple and powerful ORM for Go.

# Features

* Struct <-> Table Mapping Support
* Transaction Support
* Both ORM and raw SQL operation Support
* Chainable APIs
* Support `ID`, `In`, `Where`, `Limit`, `Join`, `OrderBy`, `GroupBy`, `Having`, `Table`, `SQL`, `Cols`
* Simple cascade loading support
* Support database schema（Postgres only）
* Sync database schemas support
* Query Cache speed up
* Database reverse support, See [xorm.io/reverse](https://xorm.io/reverse)
* Optimistic Locking support
* Support SQL Builder via [xorm.io/builder](https://xorm.io/builder)
* Context Cache support
* Support context logger interface

# Drivers Support

Drivers for Go's sql package which currently support database/sql includes:


* [Mysql5.*](https://github.com/mysql/mysql-server/tree/5.7) / [Mysql8.*](https://github.com/mysql/mysql-server) / [Mariadb](https://github.com/MariaDB/server) / [Tidb](https://github.com/pingcap/tidb)
  - [github.com/go-sql-driver/mysql](https://github.com/go-sql-driver/mysql)
  - [github.com/ziutek/mymysql/godrv](https://github.com/ziutek/mymysql/godrv)

* [Postgres](https://github.com/postgres/postgres) / [Cockroach](https://github.com/cockroachdb/cockroach)
  - [github.com/lib/pq](https://github.com/lib/pq)

* [SQLite](https://sqlite.org)
  - [github.com/mattn/go-sqlite3](https://github.com/mattn/go-sqlite3)

* MsSql
  - [github.com/denisenkom/go-mssqldb](https://github.com/denisenkom/go-mssqldb)

* Oracle
  - [github.com/mattn/go-oci8](https://github.com/mattn/go-oci8) (experiment)

# Installation

```go
go get xorm.io/xorm
```

# Documents

* [Manual](http://xorm.io/docs)

* [GoDoc](https://pkg.go.dev/xorm.io/xorm?tab=doc)

# Discuss

Please visit [Xorm on Google Groups](https://groups.google.com/forum/#!forum/xorm)

# Contributing

If you want to pull request, please see [CONTRIBUTING](https://gitea.com/xorm/xorm/src/branch/master/CONTRIBUTING.md)

# LICENSE

 BSD License
 [http://creativecommons.org/licenses/BSD/](http://creativecommons.org/licenses/BSD/)
