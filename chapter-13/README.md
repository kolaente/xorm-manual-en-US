# Reverse tool

reverse is a tool to generate codes from database schemas, see https://gitea.com/xorm/reverse .

## Install from source

```shell
go get xorm.io/reverse
```

You have to install CG同时你需要安装如下依赖，该工程依赖于CGO，请注意要安装CGO环境。

## Usage

see [https://gitea.com/xorm/reverse](https://gitea.com/xorm/reverse)
